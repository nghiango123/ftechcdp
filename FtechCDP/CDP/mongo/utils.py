import datetime

class Utils:
    def __init__(self):
        pass

    def get_timestamp(self, timestamp: int, format: str):

        readable_timestamp = datetime.datetime.utcfromtimestamp(
            self).strftime(format)
        return readable_timestamp

    def convert_time_to_correct_timezone(self, message_time: str):
        message_time = message_time.replace("T", " ")
        message_time = message_time.replace("+0000", " ")
        message_time = message_time.strip()
        message_time = datetime.datetime.strptime(message_time, "%Y-%m-%d %H:%M:%S")

        hours_added = datetime.timedelta(hours=7)
        message_time = message_time + hours_added
        message_time = str(message_time)
        return message_time

    def convert_list_to_string(self, list):
        return "***".join(str(x) for x in list)
