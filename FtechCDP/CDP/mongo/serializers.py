from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from .models import *


class ISerializer(serializers.ModelSerializer):
    def serializer_data(self, data, many=False):
        pass
class CommentSerializer(ISerializer):

    class Meta:
        models = FbComments
        fields = ['page_id', 'page_name', 'comment_id', 'post_id', 'album_id', 'photo_id', 'source_type', 'post_created_time', 'post_updated_time',
                  'comment_created_time', 'comment_from_id', 'comment_from_name',
                  'comment_message']

    def serializer_data(self, data, many=False):
        serializers = CommentSerializer(data, many=many)
        return serializers.data

class AlbumSerializer(ISerializer):
    
    class Meta:
        model = FbAlbums
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializer = AlbumSerializer(data, many=many)
        return serializer.data

class ConversationSerializer(ISerializer):

    class Meta:
        model = FbConversations
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializer = ConversationSerializer(data, many=many)
        return serializer.data

class PostSerializer(ISerializer):

    class Meta:
        model = FbPost
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializers = PostSerializer(data, many=many)
        return serializers.data

class PostAttachmentsSerializer(ISerializer):

    class Meta:
        model = FbPostAttachments
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializers = PostAttachmentsSerializer(data, many=many)
        return serializers.data

class ReactionSerializer(ISerializer):

    class Meta:
        model = FbReactions
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializers = ReactionSerializer(data, many=many)
        return serializers.data

class AlbumPhotoSerializer(ISerializer):


    class Meta:
        model = FbAlbumPhotos
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializers = AlbumPhotoSerializer(data, many=many)
        return serializers.data

class MessageTagsSerializer(ISerializer):

    class Meta:
        model = FbMessageTags
        fields = ['comment_id', 'comment_from_id']

    def serializer_data(self, data, many=False):
        serializers = MessageTagsSerializer(data, many=many)
        return serializers.data

class NlpProfileSerializer(ISerializer):

    class Meta:
        model = NlpProfile
        fields = '__all__'

    def serializer_data(self, data, many=False):
        serializers = NlpProfileSerializer(data, many=many)
        return serializers.data

    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content
