from abc import ABC, abstractmethod
from .utils import Utils
from .models import *
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max, Min
from .transformers import *

import datetime
import os
from .serializers import *

class IRepository(ABC):

    def __init__(self):
        self.util = Utils()
        self.commentTransformer = CommentTransformer()
        self.messageTagTransformer = MessageTagsTransformer()
        self.postTransformer = PostsTransformer()
        self.postAttachmentTransformer = PostAttachmentsTransformer()
        self.coversationTransformer = ConversationsTransformer()
        self.albumTransformer = AlbumsTransformer()
        self.albumPhotoTransformer = AlbumPhotosTransformer()

    def insert(self, data):
        pass

    def get_all(self):
        pass

    def update(self):
        pass


class AlbumRepository(IRepository):

    def __init__(self):
        super(AlbumRepository, self).__init__()
        self.albumTransformer = AlbumsTransformer()

    def insert(self, data):
        for item in data:
            if (self.albumTransformer.check_existed(item)):
                continue
            self.albumTransformer.parse(item)

    def get_all(self):
        try:
            data = FbAlbums.objects.using('mongo').all()[0:100]
            serializer = AlbumSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print("No data!")
            return
        return data

    def get_album(self):
        data_album = {}
        data = FbAlbums.objects.using('mongo').all()[0:100]
        serializer = AlbumSerializer()
        data = serializer.serializer_data(data, many=True)
        for obj in data:
            if obj['page_id'] not in data_album:
                data_album[obj['page_id']] = []
            data_album[obj['page_id']].append(obj)
        return data_album

    def update(self):
        pass

class AlbumPhotosRepository(IRepository):

    def __init__(self):
        super(AlbumPhotosRepository, self).__init__()

    def insert(self, data):
        for item in data:
            if (self.albumPhotoTransformer.check_existed(item)):
                continue
            self.albumPhotoTransformer.parse(item)

    def update(self):
        pass
    def get_all(self):
        try:
            data = FbAlbumPhotos.objects.using('mongo').all()[0:100]
            serializer = AlbumPhotoSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print('No Data!')
            return
        return data
    def get_albumPhoto(self):
        data_albumPhoto = {}
        try:
            data = FbAlbumPhotos.objects.using('mongo').all()[0:100]
            serializer = AlbumPhotoSerializer()
            data = serializer.serializer_data(data, many=True)
            for obj in data:
                if obj['page_story_id'] is None:
                    obj['page_story_id'] = 'unknow'
                if obj['album_id'] not in data_albumPhoto:
                    data_albumPhoto[obj['album_id']] = []
                data_albumPhoto[obj['album_id']].append(obj)
        except ObjectDoesNotExist:
            print('No data')

        return data_albumPhoto

class ConversationRepository(IRepository):

    def __init__(self):
        super(ConversationRepository, self).__init__()
        self.serializer = ConversationSerializer()

    def insert(self, data):
        for item in data:
            if (self.coversationTransformer.check_existed(item)):
                continue
            self.coversationTransformer.parse(item)

    def update(self):
        pass

    def get_all(self):
        try:
            data = FbConversations.objects.using('mongo').all()[0:100]
            data = self.serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print('No data')
            return
        return data

    def get_customerProfile(self):
        data_customerProfile = []
        try:
            result = (
                FbConversations.objects.using('mongo').values('participants_id_1').annotate(name=Max('participants_name_1'))
            )
        except ObjectDoesNotExist:
            print("Error")
            return
        for obj in result:
            data_customerProfile.append({'id': obj['participants_id_1'], 'name': obj['name']})
        return data_customerProfile[0:100]

    def get_message(self):
        try:
            message = FbConversations.objects.using('mongo').all()
            message = self.serializer.serializer_data(message, many=True)
        except ObjectDoesNotExist:
            print('No data')
            return
        return message

    def get_conversation(self):
        conversation = {}
        try:
            result = (
                FbConversations.objects.using('mongo').values('link').annotate(conversation_id=Max('conversation_id'), page_id=Max('page_id'), username=Max('participants_id_1'), user_id=Max('participants_name_1'))
            )
        except ObjectDoesNotExist:
            print('No data')
            return

        for obj in result:
            if obj['page_id'] not in conversation:
                conversation[obj['page_id']] = []
            conversation[obj['page_id']].append(obj)

        return conversation

    def get_channelCustomer(self):
        channel_customer = {}

        result_conversation = (
            FbConversations.objects.using('mongo').values('link').annotate(page_id=Max('page_id'), id = Max('participants_id_1'), name=Max('participants_name_1'))
        )

        for obj in result_conversation:
            if  obj['page_id'] not in channel_customer:
                channel_customer[obj['page_id']] = []
            channel_customer[obj['page_id']].append({'id': obj['id'], 'name': obj['name'], 'link': obj['link']})

        return channel_customer


class PostRepository(IRepository):

    def __init__(self):
        super(PostRepository, self).__init__()

    def insert(self, data):
        for item in data:
            if (self.postTransformer.check_existed(item)):
                continue
            self.postTransformer.parse(item)

    def update(self):
        pass

    def get_all(self):
        try:
            data = FbPost.objects.using('mongo').all()[0:100]
            serializer = PostSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print('No data')
            return
        return data

    def get_channel(self):
        channel = []
        result_post = (
            FbPost.objects.using('mongo').values('page_id').annotate(page_name=Max('page_name'))
        )
        result_conversation = (
            FbConversations.objects.using('mongo').values('page_id').annotate(page_name=Max('participants_name_2'))
        )
        for obj in result_post:
            channel.append({'id': obj['page_id'], 'name': obj['page_name']})
        for obj in result_conversation:
            channel.append({'id': obj['page_id'], 'name': obj['page_name']})
        return channel
    def get_post(self):
        post = {}
        try:
            data = FbPost.objects.using('mongo').all()
            serializer = PostSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print('No data')
            return
        for obj in data:
            if obj.page_id not in post:
                post[obj.page_id] = []
            post[obj.page_id].append(obj)
        return post


class PostAttachmentRepository(IRepository):
    def __init__(self):
        super(PostAttachmentRepository, self).__init__()

    def insert(self):
        for item in data:
            if (self.postAttachmentTransformer.check_existed(item)):
                continue
            self.postAttachmentTransformer.parse(item)

    def get_all(self):
        try:
            data = FbPostAttachments.objects.using('mongo').all()[0:100]
            serializer = PostAttachmentsSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print('No data!')
            return

        return data

    def update(self):
        pass

class MessageTagsRepository(IRepository):

    def __init__(self):
        super(MessageTagsRepository, self).__init__()

    def insert(self, data):
        for item in data:
            if (self.messageTagTransformer.check_existed(item)):
                continue
            self.messageTagTransformer.parse(item)

    def get_all(self):
        try:
            data = FbMessageTags.objects.using('mongo').all()
            serializer = MessageTagsSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print("No data!")
            return
        return data

    def get_tags(self):
        try:
            data_tags = {}
            tags = FbMessageTags.objects.using('mongo').all()
            serializers = MessageTagsSerializer()
            tags = serializers.serializer_data(tags, many=True)
            for obj in tags:
                if obj['comment_id'] not in data_tags:
                    data_tags[obj['comment_id']] = []
                data_tags[obj['comment_id']].append(obj)
        except ObjectDoesNotExist:
            print("No data!")
            return
        return tags

    def update(self):
        pass

class CommentsRepository(IRepository):

    def __init__(self):
        super(CommentsRepository, self).__init__()

    def insert(self, data):
        for item in data:
            if (self.commentTransformer.check_existed(item)):
                continue
            self.commentTransformer.parse(item)

    def get_all(self):
        try:
            data = FbComments.objects.using('mongo').all()
            serializer = CommentsSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print("No data!")
            return
        return data

    def get_comments(self):
        try:
            comment = FbComments.objects.using('mongo').all()
            serializers = CommentSerializer()
            comment = serializers.serializer_data(comment, many=True)

        except ObjectDoesNotExist:
            print("No data!")
            return

        return comment
    def update(self):
        pass

class ReactionsRepository(IRepository):

    def __init__(self):
        super(ReactionsRepository, self).__init__()

    def insert(self, data):
        print("Insert Post Reactions")
        for reaction in data:
            try:
                if reaction.get('source_type') == 'POST':
                    id = reaction.get('id')
                    post_id = reaction.get('post_id')
                    page_id = reaction.get('page_id')
                    FbReactions.objects.using('mongo').get(
                        id=id, post_id=post_id, page_id=page_id)
                    print(f"This record {id} has been existed")
                elif reaction.get('source_type') == 'ALBUM':
                    id = reaction.get('id')
                    album_id = reaction.get('album_id')
                    page_id = reaction.get('page_id')
                    FbReactions.objects.using('mongo').get(
                        id=id, album_id=album_id, page_id=page_id)
                    print(f"This record {id} has been existed")
                elif reaction.get('source_type') == 'PHOTO':
                    id = reaction.get('id')
                    photo_id = reaction.get('photo_id')
                    album_id = reaction.get('album_id')
                    page_id = reaction.get('page_id')
                    FbReactions.objects.using('mongo').get(
                        id=id, photo_id=photo_id, album_id=album_id, page_id=page_id)
                    print(f"This record {id} has been existed")
                else:
                    pass

            except ObjectDoesNotExist:
                if reaction.get('source_type') == 'POST':
                    print(
                        f"Insert Into Post Reactions Collection with id {reaction.get('id')}")
                elif reaction.get('source_type') == 'ALBUM':
                    print(
                        f"Insert Into Album Reactions Collection with id {reaction.get('id')}")
                elif reaction.get('source_type') == 'PHOTO':
                    print(
                        f"Insert Into Photo Reactions Collection with id {reaction.get('id')}")
                else:
                    break
                print(
                    f"Insert Into Post Reactions Collection with id {reaction.get('id')}")
                p_react = FbReactions()
                p_react.id = reaction.get('id')
                p_react.source_type = reaction.get('source_type')
                p_react.post_id = reaction.get('post_id')
                p_react.post_created_time = self.util.convert_time_to_correct_timezone(
                    reaction.get("post_created_time")) if reaction.get("post_created_time") else None
                p_react.post_updated_time = self.util.convert_time_to_correct_timezone(
                    reaction.get("post_updated_time")) if reaction.get("post_updated_time") else None
                p_react.album_id = reaction.get('album_id')
                p_react.album_created_time = self.util.convert_time_to_correct_timezone(
                    reaction.get("album_created_time")) if reaction.get("album_created_time") else None
                p_react.album_updated_time = self.util.convert_time_to_correct_timezone(
                    reaction.get("album_updated_time")) if reaction.get("album_updated_time") else None
                p_react.photo_id = reaction.get('photo_id')
                p_react.photo_created_time = self.util.convert_time_to_correct_timezone(
                    reaction.get("photo_created_time")) if reaction.get("photo_created_time") else None
                p_react.page_id = reaction.get('page_id')
                p_react.page_name = reaction.get('page_name')
                p_react.name = reaction.get('name')
                p_react.profile_type = reaction.get('profile_type')
                p_react.username = reaction.get('username')
                p_react.type = reaction.get('type')
                p_react.save(using="mongo")

    def get_all(self):
        try:
            data = FbReactions.objects.using('mongo').all()
            serializer = ReactionsSerializer()
            data = serializer.serializer_data(data, many=True)
        except ObjectDoesNotExist:
            print("No data!")
            return
        return data

    def update(self):
        pass

class NlpProfileRepository(IRepository):

    def __init__(self):
        super(NlpProfileRepository, self).__init__()

    def insert(self, data):
        nlp = NlpProfile()
        children = []
        products = []
        nlp.conversation_url = data['conversation_url']
        nlp.username = data['user_name']
        nlp.phone = data['phone']
        nlp.address = data['address']
        nlp.generateTimestamp = data['generate_timestamp']

        for ob in data['children']:
            tmp = {'content': '', 'text': '','reference': '', 'type': '', 'subtype': ''}
            for i in range(len(ob)):
                if i == 0:
                    tmp['content'] = ob[i]['content']
                    tmp['text'] = ob[i]['text']
                    tmp['reference'] = ob[i]['reference']
                    tmp['type'] = ob[i]['type']
                    tmp['subtype'] = ob[i]['subtype']
                    continue
                tmp['content'] = tmp['content'] + ", " + ob[i]['content']
                tmp['text'] = tmp['text'] + ", " + ob[i]['text']
                tmp['reference'] = tmp['reference'] + ', ' + ob[i]['reference']
                tmp['type'] = tmp['type'] + ', ' + ob[i]['type']
                tmp['subtype'] = tmp['subtype'] + ', ' + ob[i]['subtype']
            children.append(tmp)
        nlp.children = children

        for ob in data['products']:
            tmp = {'content': '','text': '', 'reference': '', 'type': '', 'subtype': ''}
            for i in range(len(ob)):
                if i == 0:
                    tmp['content'] = ob[i]['content']
                    tmp['text'] = ob[i]['text']
                    tmp['reference'] = ob[i]['reference']
                    tmp['type'] = ob[i]['type']
                    tmp['subtype'] = ob[i]['subtype']
                    continue
                tmp['content'] = tmp['content'] + ", " + ob[i]['content']
                tmp['text'] = tmp['text'] + ", " + ob[i]['text']
                tmp['reference'] = tmp['reference'] + ', ' + ob[i]['reference']
                tmp['type'] = tmp['type'] + ', ' + ob[i]['type']
                tmp['subtype'] = tmp['subtype'] + ', ' + ob[i]['subtype']
            products.append(tmp)
        nlp.product = products
        nlp.save(using='mongo')

    def insert_many(self, datas):
        for data in datas:
            nlp = NlpProfile()
            children = []
            products = []
            nlp.conversation_url = data['conversation_url']
            nlp.username = data['user_name']
            nlp.phone = data['phone']
            nlp.address = data['address']
            nlp.generateTimestamp = data['generate_timestamp']
            for ob in data['children']:
                tmp = {'content': '', 'text': '', 'reference': '', 'type': '', 'subtype': ''}
                for i in range(len(ob)):
                    if i == 0:
                        tmp['content'] = ob[i]['content']
                        tmp['text'] = ob[i]['text']
                        tmp['reference'] = ob[i]['reference']
                        tmp['type'] = ob[i]['type']
                        tmp['subtype'] = ob[i]['subtype']
                        continue
                    tmp['content'] = tmp['content'] + ", " + ob[i]['content']
                    tmp['text'] = tmp['text'] + ", " + ob[i]['text']
                    tmp['reference'] = tmp['reference'] + ', ' + ob[i]['reference']
                    tmp['type'] = tmp['type'] + ', ' + ob[i]['type']
                    tmp['subtype'] = tmp['subtype'] + ', ' + ob[i]['subtype']
                children.append(tmp)
            nlp.children = children

            for ob in data['products']:
                tmp = {'content': '', 'text': '', 'reference': '', 'type': '', 'subtype': ''}
                for i in range(len(ob)):
                    if i == 0:
                        tmp['content'] = ob[i]['content']
                        tmp['text'] = ob[i]['text']
                        tmp['reference'] = ob[i]['reference']
                        tmp['type'] = ob[i]['type']
                        tmp['subtype'] = ob[i]['subtype']
                        continue
                    tmp['content'] = tmp['content'] + ", " + ob[i]['content']
                    tmp['text'] = tmp['text'] + ", " + ob[i]['text']
                    tmp['reference'] = tmp['reference'] + ', ' + ob[i]['reference']
                    tmp['type'] = tmp['type'] + ', ' + ob[i]['type']
                    tmp['subtype'] = tmp['subtype'] + ', ' + ob[i]['subtype']
                products.append(tmp)
            nlp.product = products

            nlp.save(using='mongo')

    def get_all(self):
        data = NlpProfile.objects.using('mongo').all()
        data_nlpProfile = []
        for obj in data:
            tmp = {}
            tmp["conversation_url"] = obj.conversation_url
            tmp['user_name'] = obj.username
            tmp['address'] = obj.address
            tmp['children'] = {}
            for idx in range(len(obj.children)):
                s = "children" +  str(idx)
                tmp['children'][s] = []
                con = obj.children[idx]['content'].split(',')
                text = obj.children[idx]['text'].split(',')
                reference = obj.children[idx]['reference'].split(',')
                type = obj.children[idx]['type'].split(',')
                subtype = obj.children[idx]['subtype'].split(',')
                for i in range(len(con)):
                    tmp['children'][s].append({'content': con[i], 'text': text[i], 'reference': reference[i], 'type': type[i], 'subtype': subtype[i]})
            tmp['product'] = {}
            for idx in range(len(obj.product)):
                s = "product" + str(idx)
                tmp["product"][s] = []
                con = obj.product[idx]['content'].split(',')
                text = obj.product[idx]['text'].split(',')
                reference = obj.product[idx]['reference'].split(',')
                type = obj.product[idx]['type'].split(',')
                subtype = obj.product[idx]['subtype'].split(',')

                for i in range(len(con)):
                    tmp['product'][s].append({'content': con[i],'text': text[i], 'reference': reference[i], 'type': type[i], 'subtype': subtype[i]})

            tmp['generateTimestamp'] = obj.generateTimestamp
            data_nlpProfile.append(tmp)
        return data_nlpProfile








