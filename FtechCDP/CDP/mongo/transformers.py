from abc import ABC, abstractmethod
import mongo
from .models import *
from django.core.exceptions import ObjectDoesNotExist
from .utils import Utils
import uuid
import datetime


class ITransformer(ABC):
    def __init__(self):
        self.utils = Utils()

    def check_existed(self, id):
        pass

    def parse(self, obj):
        pass


class CommentTransformer(ITransformer):
    def __init__(self):
        super(CommentTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            if obj.get('source_type') == 'POST':
                comment_id = obj.get('id')
                post_id = obj.get('post_id')
                page_id = obj.get('page_id')
                FbComments.objects.using('mongo').get(
                    comment_id=comment_id, post_id=post_id, page_id=page_id)
                print(f"This record {comment_id} has been existed")
                return True
            elif obj.get('source_type') == 'ALBUM':
                comment_id = obj.get('id')
                album_id = obj.get('album_id')
                page_id = obj.get('page_id')
                FbComments.objects.using('mongo').get(
                    comment_id=comment_id, album_id=album_id, page_id=page_id)
                print(f"This record {comment_id} has been existed")
                return True
            elif obj.get('source_type') == 'PHOTO':
                comment_id = obj.get('id')
                photo_id = obj.get('photo_id')
                album_id = obj.get('album_id')
                page_id = obj.get('page_id')
                FbComments.objects.using('mongo').get(
                    comment_id=comment_id, photo_id=photo_id, album_id=album_id, page_id=page_id)
                print(f"This record {comment_id} has been existed")
                return True
            else:
                print("it was here")
                return False

        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        com = FbComments()
        com.comment_id = obj.get("id")
        com.source_type = obj.get("source_type")
        com.post_id = obj.get('post_id')
        com.post_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("post_created_time")) if obj.get("post_created_time") != None else None
        com.post_updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("post_updated_time")) if obj.get("post_updated_time") != None else None
        com.album_id = obj.get('album_id')
        com.album_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("album_created_time")) if obj.get("album_created_time") != None else None
        com.album_updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("album_updated_time")) if obj.get("album_updated_time") != None else None
        com.photo_id = obj.get('photo_id')
        com.photo_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("photo_created_time")) if obj.get("photo_created_time") != None else None
        com.page_id = obj.get("page_id")
        com.page_name = obj.get("page_name")
        com.comment_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        if "from" in obj:
            com.comment_from_id = obj.get("from").get("id")
            com.comment_from_name = obj.get("from").get("name")
        com.comment_message = obj.get("message")
        if "attachment" in obj:
            com.comment_attachment_title = obj.get(
                "attachment").get("title")
            com.comment_attachment_type = obj.get(
                "attachment").get("type")
            com.comment_attachment_url = obj.get(
                "attachment").get("url")
            com.comment_attachment_target_id = obj.get(
                "attachment").get("target").get("id")
            com.comment_attachment_target_url = obj.get(
                "attachment").get("target").get("url")
            if "media" in obj.get(
                    "attachment"):
                com.comment_attachment_height = obj.get(
                    "attachment").get("media").get("image").get("height")
                com.comment_attachment_src = obj.get(
                    "attachment").get("media").get("image").get("src")
                com.comment_attachment_width = obj.get(
                    "attachment").get("media").get("image").get("width")
        com.comment_live_broadcast_timestamp = obj.get(
            "live_broadcast_timestamp")
        com.comment_like_count = obj.get("like_count")
        com.comment_count = obj.get("comment_count")
        com.comment_permalink_url = obj.get("permalink_url")
        if "private_reply_conversation" in obj:
            com.comment_private_reply_conversation_id = obj.get(
                "private_reply_conversation").get("id")
            com.comment_private_reply_conversation_link = obj.get(
                "private_reply_conversation").get("link")
            com.comment_private_reply_conversation_updated_time = self.utils.convert_time_to_correct_timezone(
                obj.get("private_reply_conversation").get("updated_time")) if obj.get("private_reply_conversation").get("updated_time") != None else None
        if "parent" in obj:
            com.comment_parent_created_time = self.utils.convert_time_to_correct_timezone(obj.get(
                "parent").get("created_time")) if obj.get("parent").get("created_time") != None else None
            com.comment_parent_from_id = obj.get(
                "parent").get("from").get("id")
            com.comment_parent_from_name = obj.get(
                "parent").get("from").get("name")
        com.save(using='mongo')
        if "comments" in obj:
            try:
                rep_comments = obj["comments"]
            except:
                rep_comments = obj
            rep_comment_data = rep_comments["data"]
            for rep_com_item in rep_comment_data:
                rep_com = FbComments()
                rep_com.comment_id = rep_com_item.get("id")
                rep_com.parent_comment_id = obj.get("id")
                rep_com.source_type = obj.get("source_type")
                rep_com.post_id = obj.get('post_id')
                rep_com.post_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("post_created_time")) if obj.get("post_created_time") != None else None
                rep_com.post_updated_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("post_updated_time")) if obj.get("post_updated_time") != None else None
                rep_com.album_id = obj.get('album_id')
                rep_com.album_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("album_created_time")) if obj.get("album_created_time") != None else None
                rep_com.album_updated_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("album_updated_time")) if obj.get("album_updated_time") != None else None
                rep_com.photo_id = obj.get('photo_id')
                rep_com.photo_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("photo_created_time")) if obj.get("photo_created_time") != None else None
                rep_com.page_id = obj.get("page_id")
                rep_com.page_name = obj.get("page_name")
                rep_com.comment_created_time = self.utils.convert_time_to_correct_timezone(
                    rep_com_item.get("created_time")) if rep_com_item.get("created_time") != None else None
                if "from" in rep_com_item:
                    rep_com.comment_from_id = rep_com_item.get(
                        "from").get("id")
                    rep_com.comment_from_name = rep_com_item.get(
                        "from").get("name")
                rep_com.comment_message = rep_com_item.get("message")
                if "attachment" in rep_com_item:
                    rep_com.comment_attachment_title = rep_com_item.get(
                        "attachment").get("title")
                    rep_com.comment_attachment_type = rep_com_item.get(
                        "attachment").get("type")
                    rep_com.comment_attachment_url = rep_com_item.get(
                        "attachment").get("url")
                    rep_com.comment_attachment_target_id = rep_com_item.get(
                        "attachment").get("target").get("id")
                    rep_com.comment_attachment_target_url = rep_com_item.get(
                        "attachment").get("target").get("url")
                    if "media" in rep_com_item.get(
                            "attachment"):
                        rep_com.comment_attachment_height = rep_com_item.get(
                            "attachment").get("media").get("image").get("height")
                        rep_com.comment_attachment_src = rep_com_item.get(
                            "attachment").get("media").get("image").get("src")
                        rep_com.comment_attachment_width = rep_com_item.get(
                            "attachment").get("media").get("image").get("width")
                rep_com.comment_live_broadcast_timestamp = rep_com_item.get(
                    "live_broadcast_timestamp")
                rep_com.comment_like_count = rep_com_item.get("like_count")
                rep_com.comment_count = rep_com_item.get("comment_count")
                rep_com.comment_permalink_url = rep_com_item.get(
                    "permalink_url")
                if "private_reply_conversation" in obj:
                    rep_com.comment_private_reply_conversation_id = obj.get(
                        "private_reply_conversation").get("id")
                    rep_com.comment_private_reply_conversation_link = obj.get(
                        "private_reply_conversation").get("link")
                    rep_com.comment_private_reply_conversation_updated_time = self.utils.convert_time_to_correct_timezone(
                        obj.get("private_reply_conversation").get("updated_time")) if obj.get("private_reply_conversation").get("updated_time") != None else None
                if "parent" in rep_com_item:
                    rep_com.comment_parent_created_time = self.utils.convert_time_to_correct_timezone(rep_com_item.get(
                        "parent").get("created_time")) if rep_com_item.get(
                        "parent").get("created_time") != None else None
                    rep_com.comment_parent_from_id = rep_com_item.get(
                        "parent").get("from").get("id")
                    rep_com.comment_parent_from_name = rep_com_item.get(
                        "parent").get("from").get("name")
                rep_com.save(using="mongo")


class MessageTagsTransformer(ITransformer):
    def __init__(self):
        super(MessageTagsTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            if obj.get('source_type') == 'POST':
                uuid = obj.get('uuid')
                id = obj.get('id')
                post_id = obj.get('post_id')
                page_id = obj.get('page_id')
                FbMessageTags.objects.using('mongo').get(
                    uuid=uuid, id=id, post_id=post_id, page_id=page_id)
                print(f"This record {uuid} has been existed")
                return True
            elif obj.get('source_type') == 'ALBUM':
                uuid = obj.get('uuid')
                id = obj.get('id')
                album_id = obj.get('album_id')
                page_id = obj.get('page_id')
                FbMessageTags.objects.using('mongo').get(
                    uuid=uuid, id=id, album_id=album_id, page_id=page_id)
                print(f"This record {uuid} has been existed")
                return True
            elif obj.get('source_type') == 'PHOTO':
                uuid = obj.get('uuid')
                id = obj.get('id')
                photo_id = obj.get('photo_id')
                album_id = obj.get('album_id')
                page_id = obj.get('page_id')
                FbMessageTags.objects.using('mongo').get(
                    uuid=uuid, id=id, photo_id=photo_id, album_id=album_id, page_id=page_id)
                print(f"This record {uuid} has been existed")
                return True
            else:
                print("it was here")
                return False

        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        com_tag = FbMessageTags()
        com_tag.uuid = uuid.uuid4()
        com_tag.comment_id = obj.get("id")
        com_tag.source_type = obj.get("source_type")
        if "message_tags" in obj and obj.get("message_tags") != None:
            # print(obj.get("message_tags"))
            for tag_item in obj.get('message_tags'):
                com_tag.id = tag_item.get("id")
                com_tag.length = tag_item.get("length")
                com_tag.name = tag_item.get("name")
                com_tag.offset = tag_item.get("offset")
                com_tag.type = tag_item.get("type")
                if "from" in obj:
                    com_tag.comment_from_id = obj.get("from").get("id")
                    com_tag.comment_from_name = obj.get("from").get("name")
                com_tag.comment_message = obj.get("message")
                com_tag.post_id = obj.get('post_id')
                com_tag.post_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("post_created_time")) if obj.get("post_created_time") != None else None
                com_tag.post_updated_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("post_updated_time")) if obj.get("post_updated_time") != None else None
                com_tag.album_id = obj.get('album_id')
                com_tag.album_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("album_created_time")) if obj.get("album_created_time") != None else None
                com_tag.album_updated_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("album_updated_time")) if obj.get("album_updated_time") != None else None
                com_tag.photo_id = obj.get('photo_id')
                com_tag.photo_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("photo_created_time")) if obj.get("photo_created_time") != None else None
                com_tag.comment_created_time = self.utils.convert_time_to_correct_timezone(
                    obj.get("created_time")) if obj.get("created_time") != None else None
                com_tag.page_id = obj.get("page_id")
                com_tag.page_name = obj.get("page_name")
                com_tag.save(using="mongo")
        if "comments" in obj:
            try:
                rep_comments = obj["comments"]
            except:
                rep_comments = obj
            rep_comment_data = rep_comments["data"]
            for rep_com_item in rep_comment_data:
                rep_com_tag = FbMessageTags()
                rep_com_tag.uuid = uuid.uuid4()
                if "message_tags" in rep_com_item and rep_com_item.get("message_tags") != None:
                    for tag_item in rep_com_item.get('message_tags'):
                        rep_com_tag.id = tag_item.get("id")
                        rep_com_tag.length = tag_item.get("length")
                        rep_com_tag.name = tag_item.get("name")
                        rep_com_tag.offset = tag_item.get("offset")
                        rep_com_tag.type = tag_item.get("type")
                        if "from" in rep_com_item:
                            rep_com_tag.comment_from_id = rep_com_item.get(
                                "from").get("id")
                            rep_com_tag.comment_from_name = rep_com_item.get(
                                "from").get("name")
                        rep_com_tag.comment_message = rep_com_item.get(
                            "message")
                        rep_com_tag.comment_id = rep_com_item.get("id")
                        rep_com_tag.parent_comment_id = obj.get("id")
                        rep_com_tag.source_type = obj.get("source_type")
                        rep_com_tag.post_id = obj.get('post_id')
                        rep_com_tag.post_created_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("post_created_time")) if obj.get("post_created_time") != None else None
                        rep_com_tag.post_updated_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("post_updated_time")) if obj.get("post_updated_time") != None else None
                        rep_com_tag.album_id = obj.get('album_id')
                        rep_com_tag.album_created_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("album_created_time")) if obj.get("album_created_time") != None else None
                        rep_com_tag.album_updated_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("album_updated_time")) if obj.get("album_updated_time") != None else None
                        rep_com_tag.photo_id = obj.get('photo_id')
                        rep_com_tag.photo_created_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("photo_created_time")) if obj.get("photo_created_time") != None else None
                        rep_com_tag.comment_created_time = self.utils.convert_time_to_correct_timezone(
                            obj.get("created_time")) if obj.get("created_time") != None else None
                        rep_com_tag.page_id = obj.get("page_id")
                        rep_com_tag.page_name = obj.get("page_name")
                        rep_com_tag.save(using="mongo")


class PostsTransformer(ITransformer):
    def __init__(self):
        super(PostsTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            id = obj.get('id')
            page_id = obj.get('page_id')
            FbPost.objects.using('mongo').get(
                id=id, page_id=page_id)
            print(f"This record {id} has been existed")
            return True

        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        post = FbPost()
        post.id = obj.get('id')
        post.page_id = obj.get('page_id')
        post.page_name = obj.get('page_name')
        post.created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        post.updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("updated_time")) if obj.get("updated_time") != None else None
        post.story = obj.get("story")
        post.parent_id = obj.get("parent_id")
        post.permalink_url = obj.get("permalink_url")

        post.instagram_eligibility = obj.get(
            "instagram_eligibility")
        post.is_instagram_eligible = obj.get(
            "is_instagram_eligible")
        post.is_eligible_for_promotion = obj.get(
            "is_eligible_for_promotion")
        post.is_hidden = obj.get("is_hidden")
        post.is_popular = obj.get("is_popular")
        post.is_spherical = obj.get("is_spherical")

        date1 = datetime.datetime.strptime(
            post.created_time, "%Y-%m-%d %H:%M:%S")
        date2 = datetime.datetime.strptime(
            obj.get('date2'), "%Y-%m")
        if date1 < date2:
            post.message = obj.get('message')
            # attachments
            post_attachments = {}
            if "attachments" in obj and obj.get("attachments").get("data") != None:
                for att in range(len(obj["attachments"]["data"])):
                    try:
                        if "media_type" in obj["attachments"]["data"][att]:
                            post_attachments['attachments_media_type_' +
                                             str(att)] = obj["attachments"]["data"][att]["media_type"]
                        else:
                            post_attachments['attachments_media_type_' +
                                             str(att)] = None

                        if "title" in obj["attachments"]["data"][att]:
                            post_attachments['attachments_title_' +
                                             str(att)] = obj["attachments"]["data"][att]["title"]
                        else:
                            post_attachments['attachments_title_' +
                                             str(att)] = None

                        if "type" in obj["attachments"]["data"][att]:
                            post_attachments['attachments_type_' +
                                             str(att)] = obj["attachments"]["data"][att]["type"]
                        else:
                            post_attachments['attachments_type_' +
                                             str(att)] = None

                        if "url" in obj["attachments"]["data"][att]:
                            post_attachments['attachments_url_' +
                                             str(att)] = obj["attachments"]["data"][att]["url"]
                        else:
                            post_attachments['attachments_url_' +
                                             str(att)] = None

                    except:
                        post_attachments = None
                    post_attachments.update(post_attachments)
            else:
                post_attachments = {}
            post.attachments = post_attachments
            if "shares" in obj:
                post.shares = obj.get("shares").get("count")
            post.event = obj.get("event")
            post_properties = {}
            if "properties" in obj and obj.get("properties") != None:
                for pro in range(len(obj["properties"])):
                    try:
                        if "name" in obj["properties"][pro]:
                            post_properties['property_name_' +
                                            str(pro)] = obj["properties"][pro]["name"]
                        else:
                            post_properties['property_name_' +
                                            str(pro)] = None
                        if "text" in obj["properties"][pro]:
                            post_properties['property_text_' +
                                            str(pro)] = obj["properties"][pro]["text"]
                        else:
                            post_properties['property_text_' +
                                            str(pro)] = None
                    except:
                        post_properties = None
                    post_properties.update(post_properties)
            else:
                post_properties = {}
            post.properties = post_properties
            post.save(using="mongo")


class PostAttachmentsTransformer(ITransformer):
    def __init__(self):
        super(PostAttachmentsTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            target_id = obj.get('target_id')
            post_id = obj.get('post_id')
            page_id = obj.get('page_id')
            FbPostAttachments.objects.using('mongo').get(
                target_id=target_id, post_id=post_id, page_id=page_id)
            print(f"This record {id} has been existed")
            return True

        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        post = FbPostAttachments()
        post.post_id = obj.get('id')
        post.page_id = obj.get('page_id')
        post.page_name = obj.get('page_name')
        post.created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        post.updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("updated_time")) if obj.get("updated_time") != None else None
        post.page_id = obj.get("page_id")
        post.page_name = obj.get("page_name")
        date1 = datetime.datetime.strptime(
            post.created_time, "%Y-%m-%d %H:%M:%S")
        date2 = datetime.datetime.strptime(
            obj.get('date2'), "%Y-%m")
        if date1 < date2:
            attachments = obj.get("attachments").get('data')
            for item in attachments:
                subattachments = item.get('subattachments').get('data')
                if "subattachments" in item and subattachments != None:
                    for sub_item in subattachments:
                        post.title = sub_item.get('title')
                        post.type = sub_item.get('type')
                        post.url = sub_item.get('url')
                        if "target" in sub_item:
                            post.target_id = sub_item.get("target").get("id")
                            post.target_url = sub_item.get("target").get("url")
                        if "media" in sub_item and sub_item.get('media') != None:
                            if 'image' in sub_item.get('media'):
                                post.media_image_height = sub_item.get(
                                    "media").get("image").get('height')
                                post.media_image_src = sub_item.get(
                                    "media").get("image").get('src')
                                post.media_image_width = sub_item.get(
                                    "media").get("image").get('width')
            post.save(using="mongo")


class ConversationsTransformer(ITransformer):
    def __init__(self):
        super(ConversationsTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            message_id = obj.get('message_id')
            conversation_id = obj.get('conversation_id')
            page_id = obj.get('page_id')
            FbConversations.objects.using('mongo').get(
                message_id=message_id, conversation_id=conversation_id, page_id=page_id)
            print(f"This record {message_id} has been existed")
            return True

        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        con = FbConversations()
        con.conversation_id = obj.get("conversation_id")
        con.page_id = obj.get("page_id")
        con.page_name = obj.get("page_name")
        con.link = obj.get("link")
        con.updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("updated_time")) if obj.get("updated_time") != None else None
        con.message_count = obj.get("message_count")
        con.message_id = obj.get("id")
        con.participants_id_1 = obj.get("participants_id_1")
        con.participants_name_1 = obj.get("participants_name_1")
        con.participants_id_2 = obj.get("participants_id_2")
        con.participants_name_2 = obj.get("participants_name_2")
        con.senders_id_1 = obj.get("senders_id_1")
        con.senders_name_1 = obj.get("senders_name_1")
        con.senders_id_2 = obj.get("senders_id_2")
        con.senders_name_2 = obj.get("senders_name_2")
        con.mess_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        if 'from' in obj:
            con.mess_sender_id = obj.get("from").get("id")
        con.message = obj.get("message")
        if 'tags' in obj and obj.get("tags").get('data') != None:
            for item in obj.get("tags").get('data'):
                name_value = item.get('name')
                if "source" in str(name_value):
                    con.tags_source = item.get('name')

        mine_type_list = {}
        if "attachments" in obj:
            atts_data = obj.get("attachments").get("data")
            con.attachments_count = len(atts_data)
            mine_type = {}
            for att_item in range(len(atts_data)):
                try:
                    mine_type["mine_type_" +
                              str(att_item)] = atts_data[att_item].get("mime_type")
                except:
                    mine_type = {}
                mine_type.update(mine_type)
                mine_type_list.update(mine_type)
        con.mine_type_list = mine_type_list
        con.save(using='mongo')


class AlbumsTransformer(ITransformer):
    def __init__(self):
        super(AlbumsTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            album_id = obj.get('id')
            page_id = obj.get('page_id')
            updated_time = obj.get('updated_time')
            FbAlbums.objects.using('mongo').get(id=album_id, page_id=page_id)
            print(f"This record {album_id} has been existed")
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        album = FbAlbums()
        album.id = obj.get('id')
        album.page_id = obj.get('page_id')
        album.page_name = obj.get('page_name')
        album.created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        album.updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("updated_time")) if obj.get("updated_time") != None else None
        album.link = obj.get('link')
        album.type = obj.get('type')
        date1 = datetime.datetime.strptime(
            album.created_time, "%Y-%m-%d %H:%M:%S")
        date2 = datetime.datetime.strptime(
            obj.get('date2'), "%Y-%m")
        if date1 < date2:
            album.name = obj.get("name")
            album.description = obj.get("description")
            album.count = obj.get("count")
            album.event = obj.get("event")
            album.location = obj.get("location")
            album.save(using="mongo")


class AlbumPhotosTransformer(ITransformer):
    def __init__(self):
        super(AlbumPhotosTransformer, self).__init__()

    def check_existed(self, obj):
        try:
            id = obj.get('id')
            album_id = obj.get('album_id')
            page_id = obj.get('page_id')
            page_story_id = obj.get('page_story_id')
            FbAlbumPhotos.objects.using('mongo').get(
                id=id, album_id=album_id, page_id=page_id, page_story_id=page_story_id)
            print(f"This record {id} has been existed")
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        alb_photo = FbAlbumPhotos()
        alb_photo.id = obj.get('id')
        alb_photo.album_id = obj.get('album_id')
        alb_photo.album_name = obj.get('album_name')
        alb_photo.album_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("album_created_time")) if obj.get("album_created_time") != None else None
        alb_photo.album_updated_time = self.utils.convert_time_to_correct_timezone(
            obj.get("album_updated_time")) if obj.get("album_updated_time") != None else None
        alb_photo.page_id = obj.get('page_id')
        alb_photo.page_name = obj.get('page_name')
        alb_photo.link = obj.get('link')
        alb_photo.photo_created_time = self.utils.convert_time_to_correct_timezone(
            obj.get("created_time")) if obj.get("created_time") != None else None
        if alb_photo.photo_created_time != None:
            date1 = datetime.datetime.strptime(
                alb_photo.photo_created_time, "%Y-%m-%d %H:%M:%S")
            date2 = datetime.datetime.strptime(
                obj.get('date2'), "%Y-%m")
            if date1 < date2:
                alb_photo.name = obj.get('name')
                alb_photo.page_story_id = obj.get(
                    'page_story_id')
                alb_photo.place = obj.get(
                    'place') if obj.get('place') else {}
                alb_photo.height = obj.get('height')
                alb_photo.width = obj.get('width')
                alb_photo.save(using="mongo")
            else:
                alb_photo.name = obj.get('name')
                alb_photo.page_story_id = obj.get('page_story_id')
                alb_photo.place = obj.get(
                    'place') if obj.get('place') else {}
                alb_photo.height = obj.get('height')
                alb_photo.width = obj.get('width')
                alb_photo.save(using="mongo")
