from django.shortcuts import render
from rest_framework.decorators import api_view
from abc import ABC, abstractmethod
from .repositories import *
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

class IView(ABC):

    def __init__(self):
        pass

    def insert(request):
        pass

    def update(self):
        pass

    def get_all(self):
        pass


class AlbumsView(IView):

    def __init__(self):
        super(AlbumView, self).__init__()

    @api_view(['GET','POST'])
    def insert(request):
        print(request)
        if request.method == 'POST':
            albumRepostory = AlbumRepository()
            album_data = request.data
            albumRepostory.insert(album_data)
            return Response("Insert succesfully! status: 201")

        return Response("Insert not succesfully!")

    @api_view(['GET'])
    def get_all(self):
        albumRepository = AlbumRepository()
        data = albumRepository.get_all()

        return Response(data)

class AlbumsCommentView(IView):

    def __init__(self):
        super(AlbumsComment, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        albumCommentReposiotry = AlbumCommentsRepository()
        data = albumCommentReposiotry.get_all()

        return Response(data)

class AlbumPhotoView(IView):
    
    def __init__(self):
        super(AlbumPhotoView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        albumPhotoRepository = AlbumPhotosRepository()
        data = albumPhotoRepository.get_all()

        return Response(data)

class AlbumLikeView(IView):
    
    def __init__(self):
        super(AlbumLikeView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        albumLikeRepository = AlbumLikeRepository()
        data = albumLikeRepository.get_all()

        return Response(data)
class PostView(IView):
    
    def __init__(self):
        super(PostView, self).__init__()
    @api_view(['GET', 'POST'])
    def insert(request):
        pass
    @api_view(['GET'])
    def get_all(self):
        postReposiotry = PostRepository()
        data = postReposiotry.get_all()

        return Response(data)

class PostAttachmentsView(IView):

    def __init__(self):
        super(PostAttachmentsView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        postAttachmentRepository = PostAttachmentRepository()
        data = postAttachmentRepository.get_all()

        return Response(data)
class PostReactionView(IView):

    def __init__(self):
        super(PostReactionView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        postReactionRepository = PostReactionsRepository()
        data = postReactionRepository.get_all()

        return Response(data)

class PostCommentView(IView):
    
    def __init__(self):
        super(PostCommentView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        postCommentRepository = PostCommentRepository()
        data = postCommentRepository.get_all()

        return Response(data)

class ConversationView(IView):

    def __init__(self):
        super(ConversationView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        conversationRepository = ConversationRepository()
        data = conversationRepository.get_all()

        return Response(data)

class PhotoLikesView(IView):

    def __init__(self):
        super(PhotoLikesView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        photoLikeRepository = PhotoLikeRepository()
        data = photoLikeRepository.get_all()

        return Response(data)
class PhotoAttachmentView(IView):

    def __init__(self):
        super(PhotAttachmentView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        photoAttachmentRepository = PhotoAttachmentRepository()
        data = photoAttachmentRepository.get_all()

        return Response(data)
class PhotoCommentView(IView):
    def __init__(self):
        super(PhotoLikeView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert(request):
        pass

    @api_view(['GET'])
    def get_all(self):
        photoCommentRepository = PhotoCommentsRepository()
        data = photoCommentRepository.get_all()

        return Response(data)

class NlpProfileView(IView):
    def __init__(self):
        super(NlpProfileView, self).__init__()

    @api_view(['POST'])
    def insert(request):
        data = request.data
        nlpProfileRepository = NlpProfileRepository()
        nlpProfileRepository.insert(data)

        return Response("Insert successfully")
    @api_view(['POST'])
    def insert_many(request):
        data = request.data
        nlpProfileRepository = NlpProfileRepository()
        nlpProfileRepository.insert_many(data)

        return Response("Insert successfully")

    @api_view(['GET'])
    def get_all(request):
        nlpProfileRepository = NlpProfileRepository()
        data = nlpProfileRepository.get_all()

        return Response(data)





