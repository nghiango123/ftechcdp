from djongo import models
from djongo.models import JSONField
from django import forms


class FbComments(models.Model):
    comment_id = models.CharField(primary_key=True, max_length=100)
    parent_comment_id = models.CharField(max_length=100, blank=True, null=True)
    source_type = models.CharField(max_length=100, blank=True, null=True)
    post_id = models.CharField(max_length=100, blank=True, null=True)
    post_created_time = models.DateTimeField(blank=True, null=True)
    post_updated_time = models.DateTimeField(blank=True, null=True)
    album_id = models.CharField(max_length=100, blank=True, null=True)
    album_created_time = models.DateTimeField(blank=True, null=True)
    album_updated_time = models.DateTimeField(blank=True, null=True)
    photo_id = models.CharField(max_length=100, blank=True, null=True)
    photo_created_time = models.DateTimeField(blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    comment_created_time = models.DateTimeField(blank=True, null=True)
    comment_from_id = models.CharField(max_length=100, blank=True, null=True)
    comment_from_name = models.CharField(max_length=100, blank=True, null=True)
    comment_message = models.TextField(blank=True, null=True)
    comment_attachment_title = models.CharField(
        max_length=500, blank=True, null=True)
    comment_attachment_type = models.CharField(
        max_length=100, blank=True, null=True)
    comment_attachment_url = models.CharField(
        max_length=500, blank=True, null=True)
    comment_attachment_target_id = models.CharField(
        max_length=100, blank=True, null=True)
    comment_attachment_target_url = models.CharField(
        max_length=500, blank=True, null=True)
    comment_attachment_height = models.BigIntegerField(blank=True, null=True)
    comment_attachment_width = models.BigIntegerField(blank=True, null=True)
    comment_attachment_src = models.CharField(
        max_length=500, blank=True, null=True)
    comment_live_broadcast_timestamp = models.CharField(
        max_length=100, blank=True, null=True)
    comment_like_count = models.BigIntegerField(blank=True, null=True)
    comment_count = models.BigIntegerField(blank=True, null=True)
    comment_permalink_url = models.CharField(
        max_length=500, blank=True, null=True)
    comment_private_reply_conversation_id = models.CharField(
        max_length=100, blank=True, null=True)
    comment_private_reply_conversation_link = models.CharField(
        max_length=500, blank=True, null=True)
    comment_private_reply_conversation_updated_time = models.DateTimeField(
        blank=True, null=True)
    comment_parent_created_time = models.DateTimeField(blank=True, null=True)
    comment_parent_from_id = models.CharField(
        max_length=100, blank=True, null=True)
    comment_parent_from_name = models.CharField(
        max_length=100, blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_comments'


class FbMessageTags(models.Model):
    uuid = models.CharField(primary_key=True, max_length=100)
    id = models.CharField(max_length=100, blank=True, null=True)
    source_type = models.CharField(max_length=100, blank=True, null=True)
    comment_id = models.CharField(max_length=100, blank=True, null=True)
    parent_comment_id = models.CharField(max_length=100)
    comment_from_id = models.CharField(max_length=100, blank=True, null=True)
    comment_from_name = models.CharField(max_length=100, blank=True, null=True)
    comment_message = models.TextField(blank=True, null=True)
    post_id = models.CharField(max_length=100, blank=True, null=True)
    post_created_time = models.DateTimeField(blank=True, null=True)
    post_updated_time = models.DateTimeField(blank=True, null=True)
    album_id = models.CharField(max_length=100, blank=True, null=True)
    album_created_time = models.DateTimeField(blank=True, null=True)
    album_updated_time = models.DateTimeField(blank=True, null=True)
    photo_id = models.CharField(max_length=100, blank=True, null=True)
    photo_created_time = models.DateTimeField(blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    length = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    offset = models.BigIntegerField(blank=True, null=True)
    type = models.CharField(max_length=200, blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_message_tags'


class FbAlbumPhotos(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    album_id = models.CharField(max_length=100, blank=True, null=True)
    album_name = models.CharField(max_length=100, blank=True, null=True)
    album_created_time = models.DateTimeField(blank=True, null=True)
    album_updated_time = models.DateTimeField(blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    photo_created_time = models.DateTimeField(blank=True, null=True)
    page_story_id = models.CharField(max_length=100, blank=True, null=True)
    place = JSONField(blank=True, null=True)
    height = models.BigIntegerField(blank=True, null=True)
    width = models.BigIntegerField(blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_album_photos'


class FbAlbums(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    count = models.BigIntegerField(blank=True, null=True)
    event = models.CharField(max_length=100, blank=True, null=True)
    location = models.CharField(max_length=200, blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_albums'


class FbConversations(models.Model):
    conversation_id = models.CharField(max_length=100, blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    message_count = models.BigIntegerField(blank=True, null=True)
    participants_id_1 = models.CharField(max_length=100, blank=True, null=True)
    participants_name_1 = models.CharField(
        max_length=100, blank=True, null=True)
    participants_id_2 = models.CharField(max_length=100, blank=True, null=True)
    participants_name_2 = models.CharField(
        max_length=100, blank=True, null=True)
    senders_id_1 = models.CharField(max_length=100, blank=True, null=True)
    senders_name_1 = models.CharField(max_length=100, blank=True, null=True)
    senders_id_2 = models.CharField(max_length=100, blank=True, null=True)
    senders_name_2 = models.CharField(max_length=100, blank=True, null=True)
    message_id = models.CharField(primary_key=True, max_length=100)
    mess_created_time = models.DateTimeField(blank=True, null=True)
    mess_sender_id = models.CharField(max_length=100, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    tags_sources = models.CharField(max_length=100, blank=True, null=True)
    attachments_count = models.BigIntegerField(blank=True, null=True)
    mine_type_list = models.JSONField(blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_conversations'


class FbPostAttachments(models.Model):
    target_id = models.CharField(primary_key=True, max_length=100)
    target_url = models.CharField(max_length=500, blank=True, null=True)
    post_id = models.CharField(max_length=100, blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    title = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    url = models.CharField(max_length=500, blank=True, null=True)
    media_image_height = models.BigIntegerField(blank=True, null=True)
    media_image_width = models.BigIntegerField(blank=True, null=True)
    media_image_src = models.CharField(max_length=100, blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_post_attachments'


class FbPost(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=100, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    story = models.CharField(max_length=200, blank=True, null=True)
    parent_id = models.CharField(max_length=100, blank=True, null=True)
    permalink_url = models.CharField(max_length=500, blank=True, null=True)
    shares = models.BigIntegerField(blank=True, null=True)
    event = models.CharField(max_length=100, blank=True, null=True)
    instagram_eligibility = models.CharField(
        max_length=100, blank=True, null=True)
    is_instagram_eligible = models.BooleanField(blank=True, null=True)
    is_eligible_for_promotion = models.BooleanField(blank=True, null=True)
    is_hidden = models.BooleanField(blank=True, null=True)
    is_popular = models.BooleanField(blank=True, null=True)
    is_spherical = models.BooleanField(blank=True, null=True)
    attachments = models.JSONField(blank=True, null=True)
    properties = models.JSONField(blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_post'


# REACTIONS
class FbReactions(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    source_type = models.CharField(max_length=100, blank=True, null=True)
    post_id = models.CharField(max_length=100, blank=True, null=True)
    post_created_time = models.DateTimeField(blank=True, null=True)
    post_updated_time = models.DateTimeField(blank=True, null=True)
    album_id = models.CharField(max_length=100, blank=True, null=True)
    album_created_time = models.DateTimeField(blank=True, null=True)
    album_updated_time = models.DateTimeField(blank=True, null=True)
    photo_id = models.CharField(max_length=100, blank=True, null=True)
    photo_created_time = models.DateTimeField(blank=True, null=True)
    page_id = models.CharField(max_length=100, blank=True, null=True)
    page_name = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    profile_type = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'fb_reactions'



class Feature(models.Model):
    content = models.CharField(max_length=100)
    text = models.CharField(max_length=100)
    reference = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    subtype = models.CharField(max_length=100)

    class Meta:
        abstract = True

# NlpProfile
class NlpProfile(models.Model):
    id = models.AutoField(primary_key=True)
    conversation_url = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    phone = models.ArrayField(
        model_container=Feature,
    )
    address = models.ArrayField(
        model_container=Feature,
    )
    children = models.ArrayField(
        model_container=Feature
    )
    product = models.ArrayField(
        model_container=Feature
    )
    generateTimestamp = models.CharField(max_length=100)

    objects = models.DjongoManager()

    class Meta:
        managed = True
        db_table = 'nlp_profile'

