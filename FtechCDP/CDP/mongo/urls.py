from django.urls import path

from . import views

urlpatterns = [
    #table FbAlbum
    path('input/album', views.AlbumsView.insert, name="Insert album"),
    path('internal/album', views.AlbumsView.get_all, name="Get album"),

    # table FbPost
    path('input/post', views.PostView.insert, name="Insert Post"),
    path('internal/post', views.PostView.get_all, name="Get Post"),

    # Table FbConversation
    path('input/conversation', views.ConversationView.insert, name="Insert Conversation"),
    path('internal/conversation', views.ConversationView.get_all, name="Get Conversation"),

    # Table FbAlbumPhoto
    path('input/albumPhoto', views.AlbumPhotoView.insert, name="Insert AlbumPhoto"),
    path('internal/albumPhoto', views.AlbumPhotoView.get_all, name="Get album photo"),

    # Table FbAlbumLikes
    path('input/albumLike', views.AlbumLikeView.insert, name="Insert AlbumLike"),
    path('internal/albumLike', views.AlbumLikeView.get_all, name="Get Album Like"),

    # Table FbAlbumComment
    path('input/albumComment', views.AlbumsCommentView.insert, name="Insert AlbumComment"),
    path('internal/albumComment', views.AlbumsCommentView.get_all, name="Get Album Comment"),

    # Table FbPostAttachment
    path('input/postAttachment', views.PostAttachmentsView.insert, name="Insert PostAttachment"),
    path('internal/postAttachment', views.PostAttachmentsView.get_all, name="Get Post Attachment"),

    # Table FbPostComment
    path('input/postComment', views.PostCommentView.insert, name="Insert PostComment"),
    path('internal/postComment', views.PostCommentView.get_all, name="Get Post Comment"),

    # Table FbPostReaction
    path('input/postReaction', views.PostReactionView.insert, name="Insert PostReaction"),
    path('internal/postReaction', views.PostReactionView.get_all, name="Get Post Reaction"),

    # Table FbPhotoAttachment
    path('input/photoAttachment', views.PhotoAttachmentView.insert, name="Insert Photo Attachment"),
    path('internal/photoAttachment', views.PhotoAttachmentView.get_all, name="Get Photo Attachment"),

    # Table FbPhotoComment
    path('input/photoComment', views.PhotoCommentView.insert, name="Insert Photo Comment"),
    path('internal/photoComment', views.PhotoCommentView.get_all, name="Get Photo Comment"),

    # Table FbPhotoLike
    path('input/photoLike', views.PhotoLikesView.insert, name="Insert Photo Like"),
    path('internal/photoLike', views.PhotoLikesView.get_all, name="get Photo Like"),

    # Table NlpProfile
    path('input/nlpProfile', views.NlpProfileView.insert, name="Inser Profile"),
    path('input/nlpProfileMany', views.NlpProfileView.insert_many, name="Insert many Profile"),
    path('internal/nlpProfile', views.NlpProfileView.get_all, name="Get profile"),
]