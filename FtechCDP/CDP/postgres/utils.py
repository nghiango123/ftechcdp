
class Utils:

    def __init__(self):
        pass

    def writeFile(self, path, data, type='w'):
        with open(path, type) as f:
            for ob in data:
                f.write(ob)
                f.write('\n')
        print('Write file successfully!')

    def readFile(self, path):
        data = None
        with open(path, 'r') as f:
            data = f.readlines()
        if data is None:
            print('Data not found')
            return None
        print('Read data successfully!')
        return data

