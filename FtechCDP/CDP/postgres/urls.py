from django.urls import path

from . import views

urlpatterns = [
    # Table Post
    path('input/post/mongo', views.PostView.insert_from_mongodb, name="Insert Post from mongo"),
    path('internal/post', views.PostView.get_all, name="Get all Post"),
    path('input/post',views.PostView.insert, name="Insert post"),

    # Table Album
    path('input/album/mongo', views.AlbumView.insert_from_mongodb, name="Insert Album from mongo"),
    path('internal/album', views.AlbumView.get_all, name="Get all Album"),
    path('input/album', views.AlbumView.insert, name="Insert album"),

    # Table photo
    path('input/photo/mongo', views.PhotoView.insert_from_mongodb, name="Insert Photo from mongo"),
    path('internal/photo', views.PhotoView.get_all, name="Get all Photo"),
    path('input/photo', views.PhotoView.insert, name="Insert photo"),

    # Table Channel
    path('input/channel/mongo', views.ChannelView.insert_from_mongodb, name="Insert Channel from mongo"),
    path('internal/channel', views.ChannelView.get_all, name="Get all channel"),
    path('input/channel', views.ChannelView.insert, name="Insert Channel"),

    # Table Channel_Customer:
    path('input/channelCustomer/mongo', views.ChannelCustomerView.insert_from_mongodb, name="Insert Channel Customer from mongo"),
    path('internal/channelCustomer', views.ChannelCustomerView.get_all, name="Get all ChannelCustomer"),
    path('input/channelCustomer', views.ChannelCustomerView.insert, name="Insert ChannelCustomer"),

    # Table CustomerProfile:
    path('input/customerProfile/mongo', views.CustomerProfileView.insert_from_mongodb, name="Insert Customer Profile from mongo"),
    path('internal/customerProfile', views.CustomerProfileView.get_all, name="Get all Customer Profile"),
    path('input/customerProfile', views.CustomerProfileView.insert, name="Inset CustomerProfile"),

    # Table Conversation
    path('input/conversation/mongo', views.ConversationView.insert_from_mongodb, name="Insert Conversation from mongo"),
    path('internal/conversation', views.ConversationView.get_all, name='Get all conversation'),
    path('input/conversation', views.ConversationView.insert, name="Insert conversation"),

    # Table Comment
    path('input/comment/mongo', views.CommentView.insert_from_mongodb, name="Insert comment"),
    path('internal/comment', views.CommentView.get_all, name="Get Comment"),
    path('input/comment', views.CommentView.insert, name="Insert Comment"),

    # Table Reaction
    path('input/reaction/mongo', views.ReactionView.insert_from_mongodb, name="Inser Reaction"),
    path('internal/reaction', views.ReactionView.get_all, name="Get Reaction"),
    path('input/reaction', views.ReactionView.insert, name="Insert Reaction"),

    # Table Message
    path('input/message/mongo', views.MessageView.insert_from_mongodb, name="Insert Message from mongo"),
    path('internal/message', views.MessageView.get_all, name='Get all message'),
    path('internal/message/filter/', views.MessageView.get_fields, name="Get message"),
    path('internal/message_user', views.MessageView.get_message_user, name="Get message user"),
    path('internal/message_shop', views.MessageView.get_message_shop, name="Get message shop"),
    path('internal/message/time/', views.MessageView.get_message_time, name="Get message by time"),
    path('internal/message_shop/', views.MessageView.get_message_shop_id, name="Get message shop id"),
    path('internal/message_user/', views.MessageView.get_message_user_id, name="Get message user id"),
    path('internal/message/conversation/', views.MessageView.get_message_conversation, name="Get message conversation"),
    path('input/message', views.MessageView.insert, name="Insert Message"),
]
