from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from .models import *
from rest_framework.decorators import api_view
from .repositories import *
from abc import ABC, abstractmethod
from rest_framework.views import APIView
from django.http.response import JsonResponse
from rest_framework import status

from datetime import datetime

# Create your views here.
class IView(APIView):

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        pass

    @api_view(['GET'])
    def get_all(self, request):
        pass

    @api_view(['POST'])
    def insert(request):
        pass

class AlbumView(IView):

    def __init__(self):
        super(AlbumView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(request):
        albumRepository = AlbumRepository()
        albumRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(request):
        albumRepository = AlbumRepository()
        data = albumRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        albumRepository = AlbumRepository()
        data = request.data
        status = albumRepository.insert(data)

        return Response(status)
class PhotoView(IView):

    def __init__(self):
        super(PhotoView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        photoRepository = PhotoRepository()
        photoRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(request):
        photoRepository = PhotoRepository()
        data = photoRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        photoRepository = PhotoRepository()
        data = request.data
        status = photoRepository.insert(data)

        return Response(status)

class MessageView(IView):

    def __init__(self):
        super(MessageView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        messageRepository = MessageRepository()
        messageRepository.insert_from_mongodb()
        return Response("Insert successfully status:201")

    @api_view(['GET'])
    def get_all(self):
        messageRepository = MessageRepository()
        data = messageRepository.get_all()
        return Response(data)

    @api_view(['GET'])
    def get_fields(request):
        fields = request.GET['fields']
        fields = fields.split(",")
        message = Message.objects.all().values(*fields)[0:1000]
        return Response(message)

    @api_view(['GET'])
    def get_message_user(request):
        message = Message.objects.filter(sender='user').values('text', 'sender')[0:1000]
        return Response(message)

    @api_view(['GET'])
    def get_message_shop(request):
        message = Message.objects.filter(sender='shop').values('text', 'sender')[0:1000]
        return Response(message)

    @api_view(['GET'])
    def get_message_time(request):
        messageRepository = MessageRepository()
        list_param = request.GET
        start = None
        end = None
        if 'start' in list_param:
            start = request.GET.get('start')
        else:
            start = datetime.min()
        if 'end' in list_param:
            end = request.GET.get('end')
        else:
            end = datetime.now()
        data = messageRepository.get_timestamp(start, end)
        return Response(data)

    @api_view(['GET'])
    def get_message_shop_id(request):
        messageRepository = MessageRepository()
        shop_id = request.GET.get("shop_id")
        data = messageRepository.get_shop_id(shop_id)
        return Response(data)

    @api_view(['GET'])
    def get_message_user_id(request):
        messageRepository = MessageRepository()
        user_id = request.GET.get('user_id')
        data = messageRepository.get_user_id(user_id)

        return Response(data)

    @api_view(['GET'])
    def get_message_conversation(request):
        messageRepository = MessageRepository()
        conversation_id = request.GET.get('conversation_id')
        data = messageRepository.get_message_conversation(conversation_id)

        return Response(data)

    @api_view(['POST'])
    def insert(request):
        messageRepository = MessageRepository()
        data = request.data
        status = messageRepository.insert(data)

        return Response(status)

class PostView(IView):

    def __init__(self):
        super(PostView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        postRepository = PostRepository()
        postRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(self):
        postRepository = PostRepository()
        data = postRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        postRepository = PostRepository()
        data = request.data
        status = postRepository.insert(data)

        return Response(status)

class ChannelView(IView):

    def __init__(self):
        super(ChannelView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        channelRepository = ChannelRepository()
        channelRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(self):
        channelRepository = ChannelRepository()
        data = channelRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        channelRepository = ChannelRepository()
        data = request.data
        status = channelRepository.insert(data)

        return Response(status)

class ChannelCustomerView(IView):

    def __init__(self):
        super(ChannelCustomerView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        channelCustomerRepository = ChannelCustomerRepository()
        channelCustomerRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(self):
        channelCustomerRepository = ChannelCustomerRepository()
        data = channelCustomerRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        channelCustomerRepository = ChannelCustomerRepository()
        data = request.data
        status = channelCustomerRepository.insert(data)

        return Response(status)

class ConversationView(IView):

    def __init__(self):
        super(ConversationView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        conversationRepository = ConversationRepository()
        conversationRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(self):
        conversationRepository = ConversationRepository()
        data = conversationRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        conversationRepository = ConversationRepository()
        data = request.data
        status = conversationRepository.insert(data)

        return Response(status)

class CustomerProfileView(IView):

    def __init__(self):
        super(CustomerProfileView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        customerProfileRepository = CustomerProfileRepository()
        customerProfileRepository.insert_from_mongodb()
        return Response("Insert successfully status: 201")

    @api_view(['GET'])
    def get_all(self):
        customerProfileRepository = CustomerProfileRepository()
        data = customerProfileRepository.get_all()
        return Response(data)

    @api_view(['POST'])
    def insert(request):
        customerProfileRepository = CustomerProfileRepository()
        data = request.data
        status = customerProfileRepository.insert(data)

        return Response(status)

class CommentView(IView):

    def __init__(self):
        super(CommentView, self).__init__()

    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        pass

    @api_view(['GET'])
    def get_all(self):
        commentRepository = CommentRepository()
        data = commentRepository.get_all()

        return Response(data)

    @api_view(['POST'])
    def insert(request):
        commentRepository = CommentRepository()
        data = request.data
        status = commentRepository.insert(data)

        return Response(status)

class ReactionView(IView):

    def __init__(self):
        super(ReactionView, self).__init__()
    
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        pass
    
    @api_view(['GET'])
    def get_all(self):
        reactionRepository = ReactionRepository()
        data = reactionRepository.get_all()

        return Response(data)

    @api_view(['POST'])
    def insert(request):
        reactionRepository = ReactionRepository()
        data = request.data
        status = reactionRepository.insert(data)

        return Response(status)



