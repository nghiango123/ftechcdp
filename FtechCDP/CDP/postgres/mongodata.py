from mongo.models import *
from mongo.repositories import *


class MongodbData:
    def __init__(self):
        self.postRepository = PostRepository()
        self.conversationRepository = ConversationRepository()
        self.albumRepository = AlbumRepository()
        self.albumPhotoRepository = AlbumPhotosRepository()
        self.commentRepository = CommentsRepository()
        self.messageTagsRepository = MessageTagsRepository()

    def data_post(self):
        return self.postRepository.get_post()

    def data_channel(self):

        return self.postRepository.get_channel()

    def data_message(self):

        return self.conversationRepository.get_message()

    def data_channelCustomer(self):

        return self.conversationRepository.get_channelCustomer()

    def data_conversation(self):

        return self.conversationRepository.get_conversation()

    def data_album(self):

        return self.albumRepository.get_album()

    def data_albumPhoto(self):

        return self.albumPhotoRepository.get_albumPhoto()

    def data_customerProfile(self):

        return self.conversationRepository.get_customerProfile()

    def data_comments(self):

        return self.commentRepository.get_comments()

    def data_messageTags(self):

        return self.messageTagsRepository.get_tags()



    def data_post_album_id(self, collection):
        list_data_post = {}
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            result = coll.aggregate([
                {
                    '$group': {
                        '_id': '$page_story_id',
                        'album': {
                            '$max': '$album_id'
                        }
                    }
                }
            ])
        for ob in result:
            list_data_post[ob['_id']] = ob['album']

        return list_data_post

    def data_comment(self, collection):
        pass

    def data_reaction(self, collection):
        pass





