from abc import ABC, abstractmethod
from .models import *
from django.core.exceptions import ObjectDoesNotExist
from .utils import *

from datetime import datetime
import uuid

class ITransformer(ABC):
    def __init__(self):
        self.utils = Utils()
    def check_existed(self, id):
        pass

    def parse(self, obj):
        pass

class MessageTranformer(ITransformer):
    def __init__(self):
        super(MessageTranformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Message.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        message = Message()
        message.id = uuid.uuid4()
        if obj['mess_sender_id'] == obj['page_id']:
            message.sender = 'shop'
        else:
            message.sender = 'user'
        message.text = obj['message']
        message.raw_id = obj['message_id']
        message.created_time = obj['mess_created_time']
        message.db_updated_at = datetime.now()
        message.db_created_at = datetime.now()
        # message.conversation = conversation
        message.source_type = obj['tags_sources']
        return message

class ConversationTransformer(ITransformer):
    def __init__(self):
        super(ConversationTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Conversation.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        conversation = Conversation()
        conversation.id = uuid.uuid4()
        conversation.name = obj['username']
        conversation.link = obj['link']
        conversation.raw_id = obj['conversation_id']
        conversation.db_created_at = datetime.now()
        conversation.db_updated_at = datetime.now()
        # conversation.channel = channel

        return conversation


class ChannelTranformer(ITransformer):
    def __init__(self):
        super(ChannelTranformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Channel.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        channel = Channel()
        channel.id = uuid.uuid4()
        channel.name = obj['name']
        channel.type = "facebook"
        channel.raw_id = obj['id']
        channel.link = "https://www.facebook.com/" + str(obj['id'])
        channel.created_time = datetime.now()
        channel.db_created_at = datetime.now()
        channel.db_updated_at = datetime.now()

        return channel

class CustomerProfileTransformer(ITransformer):
    def __init__(self):
        super(CustomerProfileTransformer, self).__init__()

    def check_existed(self, id):
        pass
    def parse(self, obj):
        customerProfile = CustomerProfile()
        customerProfile.id = uuid.uuid4()
        customerProfile.link = "https://www.facebook.com/" + str(obj['id'])
        customerProfile.name = obj['name']
        customerProfile.type = "facebook"
        customerProfile.db_created_at = datetime.now()
        customerProfile.db_updated_at = datetime.now()

        with open('postgres/data_recycle/customer_profile/uuid_of_users_id.txt', 'a', encoding='utf-8') as f:
            f.write(str(customerProfile.id))
            f.write(" ")
            f.write(obj['id'])
            f.write(" ")
            f.write(obj['name'])
            f.write("\n")

        return customerProfile

class AlbumTransformer(ITransformer):
    def __init__(self):
        super(AlbumTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Album.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False
    def parse(self, obj):
        album = Album()
        album.id = uuid.uuid4()
        album.name = obj['name']
        album.raw_id = obj['id']
        album.description = obj['description']
        album.link = obj['link']
        album.type = obj['type']
        album.media_count = obj['count']
        album.event = obj['event']
        album.location = obj['location']
        album.created_time = obj['created_time']
        album.updated_time = obj['updated_time']
        album.db_updated_at = datetime.now()
        album.db_created_at = datetime.now()

        return album

class ChannelCustomerTransformer(ITransformer):
    def __init__(self):
        super(ChannelCustomerTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = ChannelCustomer.objects.get(app_page_user_id=id)
            return True
        except ObjectDoesNotExist:
            return False
    def parse(self, obj):
        channelCustomer = ChannelCustomer()
        channelCustomer.id = uuid.uuid4()
        channelCustomer.app_page_user_id = obj['id']
        channelCustomer.name = obj['name']

        return channelCustomer

class PostTransformer(ITransformer):
    def __init__(self):
        super(PostTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Post.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        word_key = u'phát trực tiếp'
        word_key_1 = u'https://www.facebook.com/media/set'
        if obj['attachments_media_type_0'] is None:
            obj['attachments_media_type_0'] = 'text'
        elif obj['attachments_media_type_0'] == '':
            obj['attachments_media_type_0'] = 'text'
        elif word_key in obj['story']:
            obj['attachments_media_type_0'] = 'livestream'
        elif obj['attachments_media_type_0'] == 'video' and word_key not in obj['story']:
            obj['attachments_media_type_0'] = 'video'
        elif obj['attachments_media_type_0'] == 'album' and word_key_1 not in obj['attachments_url_0']:
            obj['attachments_media_type_0'] = 'timeline album'
        elif obj['attachments_media_type_0'] == 'album' and word_key_1 in obj['attachments_url_0']:
            obj['attachments_media_type_0'] = 'album'

        if obj['shares'] == '':
            obj['shares'] = 0

        post = Post()
        post.id = uuid.uuid4()
        post.type = obj['attachments_media_type_0']
        post.link = obj['permalink_url']
        post.message = obj['message']
        post.story = obj['story']
        post.share_count = obj['shares']
        post.is_instagram_eligible = obj['is_instagram_eligible']
        post.is_eligible_for_promation = obj['is_eligible_for_promotion']
        post.is_hidden = obj['is_hidden']
        post.is_popular = obj['is_popular']
        post.is_spherical = obj['is_spherical']
        post.raw_id = obj['id']
        post.created_time = obj['created_time']
        post.updated_time = obj['updated_time']
        post.db_created_at = datetime.now()
        post.db_updated_at = datetime.now()

        return post

class PhotoTransformer(ITransformer):
    def __init__(self):
        super(PhotoTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Photo.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):
        photo = Photo()
        if obj['height'] == "":
            obj['height'] = 0
        if obj['width'] == "":
            obj['width'] = 0
        if obj['photo_created_time'] == "":
            obj['photo_created_time'] = None
        photo.id = uuid.uuid4()
        photo.name = obj['album_name']
        photo.link = obj['link']
        photo.height = obj['height']
        photo.width = obj['width']
        photo.raw_id = obj['id']
        photo.created_time = obj['photo_created_time']
        photo.db_created_at = datetime.now()
        photo.db_updated_at = datetime.now()

        return photo

class CommentTransformer(ITransformer):
    def __init__(self):
        super(CommentTransformer, self).__init__()

    def check_existed(self, id):
        try:
            obj = Comment.objects.get(raw_id=id)
            return True
        except ObjectDoesNotExist:
            return False

    def parse(self, obj):

        comment = Comment()
        comment.id = uuid.uuid4()
        comment.raw_id = obj['comment_id']
        if obj['comment_count'] == "":
            comment.comment_count = 0
        else:
            comment.comment_count = obj['comment_count']

        if obj['source_type'] == "album":
            try:
                album = Album.objects.get(raw_id=obj['album_id'])
                comment.album = album
            except ObjectDoesNotExist:
                print("No Album")
        elif obj['source_type'] == "photo":
            try:
                photo = Photo.objects.get(raw_id=obj['photo_id'])
                comment.photo = photo
            except ObjectDoesNotExist:
                print("No photo")
        elif obj['source_type'] == "post":
            try:
                post = Post.objects.get(raw_id=obj['post_id'])
                comment.post = post
            except ObjectDoesNotExist:
                print("No Post")

        comment.attachments = {
            'attachment_title': obj['comment_attachment_title'],
            'attachment_type': obj['comment_attachment_type'],
            'attachment_url': obj['comment_attachment_url'],
            'attachment_target_id':obj['comment_attachment_target_id'],
            'attachment_target_url': obj['comment_attachment_target_url'],
            'attachment_height': obj['comment_attachment_height'],
            'attachment_width': obj['comment_attachment_width'],
            'attachment_src': obj['comment_attachment_src'],
        }
        comment.like_count = obj['comment_like_count']
        comment.comment_count = obj['comment_count']
        comment.created_time = obj['comment_created_time']
        if obj['comment_from_id'] == obj['page_id']:
            comment.sender = 'shop'
        else:
            comment.sender = 'user'
            try:
                channelCustomer = ChannelCustomer.objects.get(fb_uid=id)
                comment.channel_customer = channelCustomer
            except ObjectDoesNotExist:
                print("No channelCustomer")
        comment.db_created_at = datetime.now()
        comment.db_updated_at = datetime.now()
        comment.parent = obj['parent_comment_id']
        comment.link = obj['comment_permalink_url']

        return comment

class ReactionTransformer(ITransformer):
    def __init__(self):
        super(ReactionTransformer, self).__init__()

    def check_existed(self, id):
        pass

    def parse(self, obj):
        reaction = Reaction()
        reaction.id = uuid.uuid4()
        reaction.reaction_type = obj['source_type']
        reaction.db_created_at = datetime.now()
        reaction.db_updated_time = datetime.now()
        if obj['source_type'] == "album":
            try:
                album = Album.objects.get(raw_id=obj['album_id'])
                reaction.album = album
            except ObjectDoesNotExist:
                print("No album")
        elif obj['source_type'] == "post":
            try:
                post = Post.objects.get(raw_id=obj['post_id'])
                reaction.post = post
            except ObjectDoesNotExist:
                print("No post")

        elif obj['source_type'] == "photo":
            try:
                photo = Photo.objects.get(raw_id=obj['photo_id'])
                reaction.photo = photo
            except ObjectDoesNotExist:
                print("No photo")

        return reaction

