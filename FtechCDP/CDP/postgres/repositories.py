from abc import ABC, abstractmethod
from .utils import Utils
from django.core.exceptions import ObjectDoesNotExist
from .models import *
from .serializers import *
from .transformers import *
from .mongodata import *

from datetime import datetime
import uuid


class IRepository(ABC):

    def __init__(self):
        self.util = Utils()
        self.mongoData = MongodbData()
        self.channelTransformer = ChannelTranformer()
        self.conversationTransformer = ConversationTransformer()
        self.channelCustomerTransformer = ChannelCustomerTransformer()
        self.messageTransformer = MessageTranformer()
        self.photoTransformer = PhotoTransformer()
        self.postTransformer = PostTransformer()
        self.customerProfileTransformer = CustomerProfileTransformer()
        self.albumTransformer = AlbumTransformer()
        self.commentTrasformer = CommentTransformer()
        self.reactionTransformer = ReactionTransformer()


    def insert_from_mongodb(self):
        pass

    def get_all(self):
        pass
    
    def update(self):
        pass
    
    def delete(self):
        pass

    def insert(self, data):
        pass


class MessageRepository(IRepository):

    def __init__(self):
        super().__init__()

    def insert_from_mongodb(self):
        data_message = self.mongoData.data_message()
        for obj in data_message:
            print(obj)
            if (not self.conversationTransformer.check_existed(obj['conversation_id'])):
                continue
            if (self.messageTransformer.check_existed(obj['message_id'])):
                continue
            message = self.messageTransformer.parse(obj)
            message.conversation = Conversation.objects.get(raw_id=obj['conversation_id'])
            message.save()

    def get_all(self):
        message = Message.objects.all()[0:1000]
        serializer = MessageSerializer()
        data = serializer.serializer_data(message, many=True)
        return data

    def get_timestamp(self, start=datetime.min, end=datetime.now):
        start = datetime.strptime(start, '%Y-%m-%d')
        end = datetime.strptime(end, '%Y-%m-%d')
        message = Message.objects.filter(created_time__gte=start).filter(created_time__lte=end)[0:1000]
        serializer = MessageSerializer()
        data = serializer.serializer_data(message, many=True)
        return data

    def get_shop_id(self, id):
        data_message = []
        channel = Channel.objects.get(raw_id=id)
        conversation = Conversation.objects.filter(channel=channel)
        for con in conversation:
            message = Message.objects.filter(conversation=con).values('id', 'text', 'sender', 'created_time', 'conversation')
            data_message.append(message)
        return data_message[0:200]

    def get_user_id(self, id):
        data_message = []
        channel_customer = ChannelCustomer.objects.get(app_page_user_id=id)
        conversation = Conversation.objects.filter(channel_customer=channel_customer)
        for con in conversation:
            message = Message.objects.filter(conversation=con).values('id', 'text', 'sender', 'created_time', 'conversation')
            data_message.append(message)
        return data_message[0:200]

    def get_message_conversation(self, id):
        conversation = Conversation.objects.get(raw_id=id)
        message = Message.objects.filter(conversation=conversation).values('id', 'text', 'sender', 'created_time', 'conversation')

        return message

    def insert(self, data):
        try:
            obj = Message.objects.get(raw_id=data['raw_id'])
            return "Message is existed!"
        except ObjectDoesNotExist:
            messageSerializer = MessageSerializer(data=data)
            if messageSerializer.is_valid():
                messageSerializer.save()
                return "Insert successfully!"
            return "Insert fail!"


class AlbumRepository(IRepository):

    def __init__(self):
        super().__init__()

    def insert_from_mongodb(self):
        data_album = self.mongoData.data_album()
        for page_id in data_album:
            if (not self.channelTransformer.check_existed(page_id)):
                continue
            channel = Channel.objects.get(raw_id=page_id)
            for obj in data_album[page_id]:
                if (self.albumTransformer.check_existed(obj['id'])):
                    continue
                album = self.albumTransformer.parse(obj)
                album.channel = channel
                album.save()
    def get_all(self):
        album = Album.objects.all()[0:100]
        albumSerializer = AlbumSerializer()
        album = albumSerializer.serializer_data(album, many=True)

        return album

    def insert(self, data):
        try:
            obj = Album.objects.get(raw_id=data['raw_id'])
            return "Album is existed!"
        except ObjectDoesNotExist:
            albumSerializer = AlbumSerializer(data=data)
            if albumSerializer.is_valid():
                albumSerializer.save()
                return "Insert successfully!"
            return "Insert fail!"


class ChannelRepository(IRepository):

    def __init__(self):
        super().__init__()
    def insert_from_mongodb(self):
        data_channel = self.mongoData.data_channel()
        for obj in data_channel:
            if (self.channelTransformer.check_existed(obj['id'])):
                continue
            channel = self.channelTransformer.parse(obj)
            channel.save()
    def get_all(self):
        channel = Channel.objects.all()[0:100]
        channelSerializer = ChannelSerializer()
        channel = channelSerializer.serializer_data(channel, many=True)

        return channel

    def insert(self, data):
        try:
            obj = Channel.objects.get(raw_id=data['raw_id'])
            return "Channel is existed"
        except ObjectDoesNotExist:
            channelSerializer = ChannelSerializer(data=data)
            if channelSerializer.is_valid():
                channelSerializer.save()
                return "Insert successfully"
            return "Insert fail!"

class ChannelCustomerRepository(IRepository):
    def __init__(self):
        super().__init__()
    def insert_from_mongodb(self):
        data_channelCustomer = self.mongoData.data_channelCustomer()
        for page_id in data_channelCustomer:
            if (not self.channelTransformer.check_existed(page_id)):
                continue
            channel = Channel.objects.get(raw_id=page_id)
            for obj in data_channelCustomer[page_id]:
                if (self.channelCustomerTransformer.check_existed(obj['id'])):
                    continue
                channelCustomer = self.channelCustomerTransformer.parse(obj)
                channelCustomer.channel = channel
                channelCustomer.save()
    def get_all(self):
        channelCustomer = ChannelCustomer.objects.all()[0:1000]
        serializer = ChannelCustomerSerializer()
        channelCustomer = serializer.serializer_data(channelCustomer, many=True)

        return channelCustomer

    def insert(self, data):
        try:
            obj = Channel.objects.get(app_page_user_id=data['app_page_user_id'])
            return "Channel customer is existed!"
        except ObjectDoesNotExist:
            channelCustomerSerializer = ChannelCustomerSerializer(data=data)
            if channelCustomerSerializer.is_valid():
                channelCustomerSerializer.save()
                return "Insert successfully"
            return "Insert fail"

class PhotoRepository(IRepository):

    def __init__(self):
        super(PhotoRepository, self).__init__()

    def insert_from_mongodb(self):
        data_photo = self.mongoData.data_albumPhoto()
        for album_id in data_photo:
            album = None
            if (self.albumTransformer.check_existed(album_id)):
                album = Album.objects.get(raw_id=album_id)
            for obj in data_photo[album_id]:
                if (self.photoTransformer.check_existed(obj['id'])):
                    continue
                albumPhoto = self.photoTransformer.parse(obj)
                albumPhoto.album = album
                albumPhoto.save()
    def get_all(self):
        photo = Photo.objects.all()[0:100]
        serializer = PhotoSerializer()
        photo = serializer.serializer_data(photo, many=True)

        return photo

    def insert(self, data):
        try:
            obj = Photo.objects.get(raw_id=data['raw_id'])
            return "Photo is existed!"
        except ObjectDoesNotExist:
            photoSerializer = PhotoSerializer(data=data)
            if photoSerializer.is_valid():
                photoSerializer.save()
                return "Insert successfully"
            return "Insert fail!"
class ConversationRepository(IRepository):

    def __init__(self):
        super(ConversationRepository, self).__init__()

    def insert_from_mongodb(self):
        data_conversation = self.mongoData.data_conversation()
        for page_id in data_conversation:
            if (self.channelTransformer.check_existed(page_id)):
                channel = Channel.objects.get(raw_id=page_id)
            else:
                print('No channel!')
                continue
            for obj in data_conversation[page_id]:
                if (self.conversationTransformer.check_existed(obj['conversation_id'])):
                    print("Conversation exists")
                conversation = self.conversationTransformer.parse(obj)
                conversation.channel = channel
                if (self.channelCustomerTransformer.check_existed(obj['user_id'])):
                    channelCustomer = ChannelCustomer.objects.get(app_page_user_id=user_id)
                    conversation.channel_customer = channelCustomer
                conversation.save()
    def get_all(self):
        conversation = Conversation.objects.all()[0:100]
        serializer = ConversationsSerializer()
        conversation = serializer.serializer_data(conversation, many=True)

        return conversation

    def insert(self, data):
        try:
            obj = Conversation.objects.get(raw_id=data['raw_id'])
            return "Conversation is existed!"
        except ObjectDoesNotExist:
            conversationSerializer = ConversationsSerializer(data=data)
            if conversationSerializer.is_valid():
                conversationSerializer.save()
                return "Insert conversation successfully!"
            return "Insert fail!"

class PostRepository(IRepository):

    def __init__(self):
        super(PostRepository, self).__init__()

    def insert_from_mongodb(self):
        data_post = self.mongoData.data_post()
        for page_id in data_post:
            if (not self.channelTransformer.check_existed(page_id)):
                continue
            channel = Channel.objects.get(raw_id=page_id)
            for obj in data_post[page_id]:
                if (self.postTransformer.check_existed(obj['id'])):
                    continue
                post = self.postTransformer.parse(obj)
                post.channel = channel
                post.save()

    def get_all(self):
        post = Post.objects.all()[0:100]
        serializer = PostSerializer()
        post = serializer.serializer_data(post, many=True)

        return post

    def insert(self, data):
        try:
            obj = Post.objects.get(raw_id=data['raw_id'])
            return "Post is existed!"
        except ObjectDoesNotExist:
            postSerializer = PostSerializer(data=data)
            if postSerializer.is_valid():
                postSerializer.save()
                return "Insert post successfully!"
            return "Insert fail!"
class CustomerProfileRepository(IRepository):

    def __init__(self):
        super(CustomerProfileRepository, self).__init__()

    def insert_from_mongodb(self):
        data_customerProfile = self.mongoData.data_customerProfile()
        for obj in data_customerProfile:
            customerProfile = self.customerProfileTransformer.parse(obj)
            customerProfile.save()

    def get_all(self):
        customerProfile = CustomerProfile.objects.all()[0:1000]
        serializer = CustomerProfileSerializer()
        customerProfile = serializer.serializer_data(customerProfile, many=True)

        return customerProfile

    def insert(self, data):
        customerProfileSerializer = CustomerProfileSerializer(data=data)
        if customerProfileSerializer.is_valid():
            customerProfileSerializer.save()
            return "Insert CustomerProfile successfully!"
        return "Insert fail!"

class CommentRepository(IRepository):
    
    def __init__(self):
        super(CommentRepository, self).__init__()
    
    def insert_from_mongodb(self):
        data_comments = self.mongoData.data_comments()
        data_messageTags = self.mongoData.data_messageTags()

        for obj in data_comments:
            if self.commentTrasformer.check_existed(obj['comment_id']):
                continue
            comment = self.commentTrasformer.parse(obj)
            data_tags = data_messageTags[obj['comment_id']]
            for ob in data_tags:
                comment.tagged_customer.add(ob)

            comment.save()
    
    def get_all(self):

        comment = Comment.objects.all()[0:1000]
        serializer = CommentSerializer()
        comment = serializer.serializer_data(comment, many=True)

        return comment
class ShopRepository(IRepository):
    
    def __init__(self):
        super(ShopRepository, self).__init__()

    def insert_from_mongodb(self):
        pass

    def get_all(self):
        try:
            shop = Shop.objects.all()[0:1000]
            shopSerializer = ShopSerializer()
            shop = shopSerializer.serializer_data(shop, many=True)
        except:
            return
        return shop

class ReactionRepository(IRepository):
    
    def __init__(self):
        super(ReactionRepository, self).__init__()

    def insert_from_mongodb(self):
        data_reaction = self.mongoData.data_reaction()
        for obj in data_reaction:
            reaction = self.reactionTransformer.parse(obj)
            reaction.save()

    def get_all(self):

        reaction = Reaction.objects.all()[0:1000]
        serializer = ReactionSerializer()
        reaction = serializer.serializer_data(reaction, many=True)

        return reaction