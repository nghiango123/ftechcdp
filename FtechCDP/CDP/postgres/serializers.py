from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from .models import *
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from .mongodb import MongodbData


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = PostSerializer(data, many=many)
        return data_serializer.data

    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content

    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = MessageSerializer(data, many=many)
        return data_serializer.data

    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content

    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data

class ConversationsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Conversation
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = ConversationsSerializer(data, many=many)
        return data_serializer.data

class ChannelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = ChannelSerializer(data, many=many)
        return data_serializer.data
class ChannelCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChannelCustomer
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = ChannelCustomerSerializer(data, many=many)
        return data_serializer.data

class CustomerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerProfile
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = CustomerProfileSerializer(data, many=many)
        return data_serializer.data

class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = PhotoSerializer(data, many=many)
        return data_serializer.data

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = CommentSerializer(data, many=many)
        return data_serializer.data

class ReactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reaction
        fields = '__all__'
    def serializer_data(self, data, many=False):
        data_serializer = ReactionSerializer(data, many=many)
        return data_serializer.data

class ShopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shop
        fields = '__all__'
    def serializer_data(self, data, many=False):
        data_serializer = ShopSerializer(data, many=many)
        return data_serializer.data

class AlbumSerializer(serializers.ModelSerializer):

    class Meta:
        model = Album
        fields = '__all__'
    def serializer_data(self, data, many=False):
        data_serializer = AlbumSerializer(data, many=many)
        return data_serializer.data
